########### CPF ###########
# from validate_docbr import CPF
# from cpf import Cpf
# cpf = '15616987913'
# obj_cpf = Cpf(cpf)
# valido = '01234567890'
# invalido 012.345.678-91
# cpf = CPF()
# print(cpf.validate('012.345.678-90'))
# cpf_um = Cpf('12354367996')
# print(cpf_um)

########### CNPJ ###########
# from cpf_cnpj import CpfCnpj
# # from validate_docbr import CNPJ
# # cnpj = CNPJ()
# exemplo_cnpj = '59065147000102'
# # print(cnpj.validate(exemplo_cnpj))
# documento = CpfCnpj(exemplo_cnpj, 'cnpj')
# print(documento)

########### FACTORY #############
# from cpf_cnpj import Documento
# documento_cnpj = Documento.cria_documento(exemplo_cnpj)
# print(documento_cnpj)
# documento_cpf = Documento.cria_documento(valido)
# print(documento_cpf)

########### TELEFONE ############
# from telefones_br import TelefonesBr
# import re
# padrao_molde = '(xx)aaaa-wwww'
# padrao = '[0-9]{2}[0-9]{4,5}[0-9]{4}'
# texto = 'eu gosto do numero 19995376756 e também do 21573597440'
# resposta = re.findall(padrao, texto)
# print(resposta)
# telefone_obj = TelefonesBr(551123456789)
# print(telefone_obj)

########### DATAS ############
from datetime import datetime, timedelta
# from datas_br import DatasBr
# print(datetime.today())
# cadastro = DatasBr()
# print(cadastro.momento_cadastro)
# print(cadastro.dia_semana())
# print(cadastro)

########### CEP ############
from acesso_cep import BuscaEndereco
# import requests
# r = requests.get('https://viacep.com.br/ws/01001000/json/')
# json_r = r.json()
# print(json_r)
# print(type(json_r))
cep = BuscaEndereco('01001000')
print(cep)
