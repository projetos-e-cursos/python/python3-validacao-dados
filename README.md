# Python3 - Validação de Dados nos padrões BR

### Conceitos abordados:
- Classes;
- Validações e tratativas;
- Instalação de pacotes externos do pypi;
- Factory;
- Métodos estáticos;
- Expressões regulares;
- re search, fidall e group;
- datetime;
- date;
- timedelta;
- Manipulação e formatação de datas;
- APIs;
- WebServices;
- Acesso a resposta da API;
- resp.json();
