import re


class TelefonesBr:
    def __init__(self, telefone):
        telefone = str(telefone)
        if self.valida_telefone(telefone):
            self.numero = telefone
        else:
            raise ValueError('Número inválido!!')

    def __str__(self):
        return self.format_numero()

    @staticmethod
    def valida_telefone(telefone):
        padrao = '([0-9]{2,3})?([0-9]{2})([0-9]{4,5})([0-9]{4})'
        resposta = re.findall(padrao, telefone)
        if resposta:
            return True
        else:
            return False

    def format_numero(self):
        padrao = r'([\d]{2,3})?([\d]{2})([\d]{4,5})([\d]{4})'
        res = re.search(padrao, self.numero)
        numero_formatado = f'+{res.group(1)}({res.group(2)}){res.group(3)}-{res.group(4)}'
        return numero_formatado
